exports.getMinute = (timestamp) => {
    const milliSecond = timestamp * 1000;
    const dateObject = new Date(milliSecond);
    const minute = dateObject.toLocaleString('en-us',{minute : "number"});
    return minute;
};

exports.getHour = (timestamp) => {
    const milliSecond = timestamp * 1000;
    const dateObject = new Date(milliSecond);
    const hour = dateObject.toLocaleString('en-us',{hour : "number"});
    return hour;
};

exports.getDay = (timestamp) => {
    const milliSecond = timestamp ;
    const dateObject = new Date(milliSecond);
    
    const day = dateObject.getDate();
    return day;
};

exports.getMonth = (timestamp) => {
    const milliSecond = timestamp ;
    const dateObject = new Date(milliSecond);
    const month = dateObject.getMonth();
    return month;
};
exports.getYear = (timestamp) => {
    const milliSecond = timestamp ;
    const dateObject = new Date(milliSecond);
    const year = dateObject.getFullYear();
    return year;
};

exports.fullTime = (timestamp) => {
        let day = this.getDay(timestamp);
        const year = this.getYear(timestamp);
        let month = this.getMonth(timestamp);
        month ++
        return `${year}`+'-'+`${month<=9 ? '0'+ month : month }` + '-' + `${day<=9 ? '0'+ day : day }`;
};