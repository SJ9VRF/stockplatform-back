var mongoose = require('mongoose');
var config = require('./config');
var mongoClient = require('mongodb').MongoClient;
let db;

exports.connect = (collectionName) => {
    if(db){
        console.log('client');
        return db.collection(collectionName);
    }
    else{
        mongoClient.connect(config.priceDataURL).then((cl) => {
            console.log('Connecting Correctly')
            db =cl.db('db-data-stock');
            return db.collection();
        })
    }

    
}