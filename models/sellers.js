const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    value : {
        type : Number
    },
    unitPrice : {
        type : Number
    },
    seller : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    }
}, {timestamps : true})
const sellerSchema = new Schema({
    currencyName : {
        type : String
    },
    orders : [orderSchema]
}, {timestamps : true});

module.exports = mongoose.model('Seller', sellerSchema);
