const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const priceSchema = new Schema({
    price : {
        type : Number
    }
}, {timestamps : true})
const currencySchema = new Schema({
    name : {
        type : String
    },
    prices : [priceSchema]
}, {timestamps : true})

module.exports = mongoose.model('Currency',currencySchema);