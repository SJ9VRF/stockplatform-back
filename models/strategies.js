const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const nodeSchema = new Schema({
    id : {
        type : String,
        required: true,
        
    },
    name : {
        type : String,
        required : true
    },
    NodeX  : {
        type : Number ,
        required : true,
    },
    NodeY : {
        type : Number,
        required : true , 
    },
    type : { type : String},
    timeframe : {type : String},
    fastPeriod : {type : Number},
    slowPeriod : {type : Number},
    signalPeriod : {type : Number},
    SimpleMAOscillator: {type : Number},
    SimpleMASignal : {type : Boolean},
    period :{type : Number},
    stdDev : {type : Number},
    practicalPrice : {type : String},
    volume :{type : [Number]},
    isReady : {type : Boolean, default : false},
    kPeriod : {type : Number},
    dPeriod : {type : Number},
    stochasticPeriod : {type : Number},
    rsiPeriod :{type : Number},
    averageMethod : {type : String , default: 'SMA'},
    mathMethod : {type : String , default : 'min' },
    conversionPeriod :{type : Number},
    basePeriod : {type : Number},
    spanPeriod : {type : Number},
    displacement : {type : Number},
    comparisonValue1 : {type : Number},
    comparisonValue2 : {type : Number}
} );

const sourceSchema = new Schema({
    id : {type : String},
    name : {type : String}
})
const targetSchema = new Schema({
    id : {type : String},
    name : {type : String}
})
const linkSchema = new Schema ({
    source : {
        type : String
    },
    target : {
        type : String
    } 
});

const strategySchema = new Schema({
    Name : {
        type : String , 
        required: true,
        unique : true
    },
    Nodes : [nodeSchema] ,
    Links : [linkSchema],
    maker : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    forSell : {
        type : Boolean,
        default : false
    },
    price : {
        type : Number,
        default : 0
    }
}, {timestamps : true});

module.exports.Strategy = mongoose.model('Strategy' , strategySchema);
module.exports.Node = mongoose.model('Node', nodeSchema);