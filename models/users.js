const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const digitalWalletSchema = new Schema({
    currencyName : {
        type : String
    },
    value : {
        type : Number
    }
});
const userSchema = new Schema({
    username : {
        type : String,
        sparse : true ,
        unique : true
    },
    firstName : {
        type: String
    },
    lastName : {
        type : String
    },
    email : {
        type : String,
        unique : true
    },
    telNumber : {
        type : Number
    },
    nationalCode : {
        type : Number,
        sparse : true,
        unique : true
    },
    admin : {
        type:Boolean,
        default : false
    },
    image : {
        type : String
    },
    wallet: {
        type : Number,
        default : 0
    },
    rate : {
        type : Number,
        default : 0
    },
    shabaCode : {
        type : Number
    },
    cardNumber : {
        type : Number
    },
    birthDay : {
        type : String
    },
    status : {
        type : String
    },
    address : {
        type : String
    },
    documnetPath : {
        type : String
    },
    description : {
        type: String
    },
    purchased : [String],
    digitalWallet : [digitalWalletSchema]
}, {timestamps : true});

userSchema.plugin(passportLocalMongoose,{usernameField : 'email'});

module.exports = mongoose.model('User' , userSchema);