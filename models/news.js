const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    author : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    content : {
        type : String
    }
},
{timestamps:true})

const newsSchema = new Schema({
    image : {
        type : String
    },
    title: {
        type : String
    },
    titleEn : {
        type : String
    },
    content : {
        type : String
    },
    contentEn : {
        type : String
    },
    likes:{
        type: Number
    },
    hates:{
        type: Number
    },
    comments : [commentSchema]
}, {timestamps: true});

module.exports = mongoose.model('New' , newsSchema);