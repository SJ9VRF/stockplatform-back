const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    value : {
        type : Number
    },
    unitPrice : {
        type : Number
    },
    shopper : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    }
}, {timestamps : true})
const shopperSchema = new Schema({
    currencyName : {
        type : String
    },
    orders : [orderSchema]
    
}, {timestamps : true});

module.exports= mongoose.model('Shopper', shopperSchema);
