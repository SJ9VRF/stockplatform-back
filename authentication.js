const passport = require('passport');
const passportLocal = require('passport-local');
const LocalStrategy = passportLocal.Strategy;

const passportJWT = require('passport-jwt');
const passportJWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const JWT = require('jsonwebtoken');

const User = require('./models/users');
const config = require('./config');

exports.local = passport.use(new LocalStrategy({usernameField : 'email'},User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
    
exports.getToken = (user) => {
    return JWT.sign(user, config.secretKey,{ expiresIn : "2 days"});
};

opt = {
    secretOrKey : config.secretKey,
    jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken()
};


exports.jwtAuth = passport.use(new passportJWTStrategy(opt , (payload ,done) => {

    User.findOne({_id : payload._id},(err , user) => {
        if(err){
            return done(err,false);
        }
        else if(user){
            return done(null , user);
        }
        else{
            return done(null, false);
        }
    });
}));

exports.verifyUser = passport.authenticate('jwt', {session : false});

exports.verifyAdmin = (req, res, next) => {
    if(req.user.admin){
        return next();
    }
    else{
        let err = new Error('Only Admin can access to this web page or resources!');
        err.status = 403;
        return next(err);
    }
}