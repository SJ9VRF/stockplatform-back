//just for test
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Strategies = require('../models/strategies').Strategy;
const authentication = require('../authentication');
const Time = require('../time');
const strategyRouter = express.Router();
strategyRouter.use(bodyParser.json());
const appDb = require('../app').db;
const connection = require('../dbConnection');
const mongoClient = require('mongodb').MongoClient;
const config = require('../config');
const User = require('../models/users');

strategyRouter.route('/')
.options(cors.corsWithOptions , authentication.verifyUser, (req, res, next) => res.statusCode = 200)
.get(cors.corsWithOptions, authentication.verifyUser,(req,res, next) => {
    Strategies.find({maker : req.user._id}).then((strategies =>{
        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.json(strategies);
    }),(err) => next(err))
    .catch((err) => next(err))
})
.post(cors.cors,authentication.verifyUser, (req, res, next) => {
    console.log('req.body' , req.body); 
    req.body.maker = req.user._id;
    Strategies.create({Name : req.body.Name , Nodes : req.body.Nodes , Links: req.body.Links, maker: req.body.maker})
    .then((strategy) => {
        if(strategy){
            res.statusCode = 200;
            res.setHeader('Content-Type' , 'application/json');
            res.json(strategy);
        }
        else{
            res.statusCode = 200;
            res.setHeader('Content-Type' , 'text/plain');
            res.end('empty');
        }
        
    }, (err) =>{ console.log('errrr', err);next(err);})
    .catch((err) => { console.log('err chatch', err);next(err);});
});
strategyRouter.route('/st/purchased')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.post(cors.corsWithOptions,authentication.verifyUser, (req, res, next) => {
        User.findById(req.user._id).then((user) => {
            user.purchased.push(req.body.purchasedName);
            user.save().then((user) => {
                statusCode = 200;
                res.setHeader('Content-Type','application/json');
                res.json(user);
            },(err) => next(err)).catch((err) => next(err))
        },(err) => next(err)).catch((err) => next(err))
})

strategyRouter.route('/:strategyName')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.get(cors.corsWithOptions,authentication.verifyUser, (req, res, next) => {
    console.log('req.params.strategyNamr' , req.params.strategyName);
    Strategies.findOne({Name : req.params.strategyName}, (err, resp) =>{
        if(err){
            console.log('eerr' , err);
            return next(err);
        }
        else{
            console.log('resp', JSON.stringify(resp));
            res.statusCode = 200;
            res.setHeader('Content-Type' , 'application/json');
            res.json(resp);
        }
    })
    
});
strategyRouter.route('/forSell/:strategyId')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    req.body.maker = req.user._id;
    Strategies.findByIdAndUpdate( req.params.strategyId,{$set : req.body}).then((strategy) => {
            res.statusCode =200;
            res.setHeader('Content-Type' , 'application/json');
            res.json(strategy);
    }, (err) => next(err)).catch((err) => next(err));
})
strategyRouter.route('/forSell/strategy')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Strategies.find(req.query).populate("maker").then((strategies) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(strategies);
    }, (err) => next(err)).catch((err) => next(err));
})
strategyRouter.route('/collection/collectionNames')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    mongoClient.connect(config.priceDataURL).then((client) => {
         client.db('db-data-stock').listCollections().toArray().then((collections) => {
            collections = collections.map((col) => col.name);
            res.statusCode = 200; 
            res.send(collections)
         })
         client.close();

         
    })    
})
strategyRouter.route('/collection/collections')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    mongoClient.connect(config.priceDataURL).then((client) => {
         client.db('db-data-stock').listCollections().toArray().then((collections) => {
            collections = collections //.map((col) => col._id);
            res.statusCode = 200; 
            res.send(collections)
         })
         client.close();

         
    })    
})
strategyRouter.route('/dateVolume')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.post(cors.corsWithOptions , authentication.verifyUser, (req, res, next) => {
    mongoClient.connect(config.priceDataURL).then((client) => {
        console.log('collectionName', req.body.collectionName);
        let doc = client.db('db-data-stock').collection(req.body.collectionName);
        doc.find({}).toArray((err , prices) => {
            if(err){
                return next(err)
            }
            prices = prices.sort((a,b) => b.time -a.time  );
            if(req.body.startDate && req.body.endDate){
                prices = prices.filter((price)=> {
                    return (price.time < req.body.startDate && price.time > req.body.endDate )
                })
            }
            else{
                let month = Time.getMonth(prices[0].time);
                let year = Time.getYear(prices[0].time);
                prices = prices.filter((price)=> {
                    if (Math.abs(Time.getMonth(price.time) - month) <= 6 && 
                    Time.getYear(price.time) == year){
                        return true
                    }
                })
            }
            let volume = 0;
            for(let price of prices){
                volume += price.volume;
            }
            res.statusCode =200;
            client.close();
            res.send({volume : volume, startDate : prices[0].time, endDate: prices[prices.length-1].time});
        })  
    })
})
strategyRouter.route('/exchangeData/:timeframe')
.options(cors.corsWithOptions , (req, res, next) => res.statusCode = 200)
.post(cors.corsWithOptions , authentication.verifyUser, (req, res, next) => {
    mongoClient.connect(config.priceDataURL).then((client) => {
        console.log('collectionName', req.body.collectionName);
        let doc = client.db('db-data-stock').collection(req.body.collectionName);
        
        switch (req.params.timeframe) {
            case 'Daily':
                doc.find({}).toArray((err , prices) => {
                    if(err){
                        return next(err)
                    }
                    prices = prices.sort((a,b) => b.time -a.time  );
                    if(req.body.startDate && req.body.endDate){
                        prices = prices.filter((price)=> {
                            return (price.time < req.body.startDate && price.time > req.body.endDate )
                        })  
                    }
                    else{
                        let month = Time.getMonth(prices[0].time);
                        let year = Time.getYear(prices[0].time);
                        prices = prices.filter((price)=> {
                            if (Math.abs(Time.getMonth(price.time) - month) <= 6 && 
                            Time.getYear(price.time) == year){
                                return true
                            }
                        })
                    }
                    let values = calculateForDay(prices ,req.body.practicalPrice);
                    res.statusCode =200;
                    client.close();
                    res.send(values);
                })  
                break;
            case 'Monthly':
                doc.find({}).toArray((err, prices) => {
                    if(err){
                        return next(err);
                    }
                    prices = prices.sort((a,b) => b.time -a.time  );
                    if(req.body.startDate && req.body.endDate){
                        prices = prices.filter((price)=> {
                            return (price.time < req.body.startDate && price.time > req.body.endDate )
                        })
                    }
                    else{
                        let month = Time.getMonth(prices[0].time);
                        let year = Time.getYear(prices[0].time);
                        prices = prices.filter((price)=> {
                            if (Math.abs(Time.getMonth(price.time) - month) <= 6 && 
                            Time.getYear(price.time) == year){
                                return true
                            }
                        })
                    }
                    let values = calculateForMonth(prices, req.body.practicalPrice);
                    res.statusCode = 200;
                    client.close();
                    res.send(values);
                })
                break;
            case 'Weekly':
                doc.find({}).toArray((err, prices) => {
                    if(err){
                        return next(err);
                    }
                    prices = prices.sort((a,b) => b.time -a.time  );
                    if(req.body.startDate && req.body.endDate){
                        prices = prices.filter((price)=> {
                            return (price.time < req.body.startDate && price.time > req.body.endDate )
                        })
                    }
                    else{
                        let month = Time.getMonth(prices[0].time);
                        let year = Time.getYear(prices[0].time);
                        prices = prices.filter((price)=> {
                            if (Math.abs(Time.getMonth(price.time) - month) <= 6 && 
                            Time.getYear(price.time) == year){
                                return true
                            }
                        })
                    }
                    let values = calculateForWeek(prices, req.body.practicalPrice);
                    res.statusCode = 200;
                    client.close();
                    res.send(values);
                })
                break;
            case 'Current':
                doc.find({}).toArray((err, prices) => {
                    if(err){
                        return next(err);
                    }
                    prices = prices.sort((a,b) => b.time -a.time  );
                    if(req.body.startDate && req.body.endDate){
                        prices = prices.filter((price)=> {
                            return (price.time < req.body.startDate && price.time > req.body.endDate )
                        })
                    }
                    else{
                        let month = Time.getMonth(prices[0].time);
                        let year = Time.getYear(prices[0].time);
                        prices = prices.filter((price)=> {
                            if (Math.abs(Time.getMonth(price.time) - month) <= 6 && 
                            Time.getYear(price.time) == year){
                                return true
                            }
                        })
                    }
                    let values = calculateForCurrent(prices, req.body.practicalPrice);
                    res.statusCode = 200;
                    client.close();
                    res.send(values);
                })
                break;
            default:
                break;
        }
    })
    
})


function calculateForDay(prices , practicalPrice){
    switch (practicalPrice) {
        case 'Open':
            let day = Time.getDay(prices[0].time);
            let month = Time.getMonth(prices[0].time);
            let values = [];
            for(let price of prices){
                if(Time.getDay(price.time) != day){
                    day = Time.getDay(price.time);
                    values.push({price : price.open, time: Time.fullTime(price.time)});
                }
            }
            return values;
            break;
        case 'Low':
            let min = prices[0].low;
            let ld = Time.getDay(prices[0].time);
            let lm = Time.getMonth(prices[0].time);
            let lv = [];
            for(let price of prices){
                if(Time.getDay(price.time) != ld){
                    ld = Time.getDay(price.time);
                    lv.push({price : min, time : Time.fullTime(price.time)});
                    min = price.low;
                }
                if(price.low < min){
                    min = price.low;
                }

            }
            return lv;
            break;
        case 'High':
            let max = prices[0].high;
            let hd = Time.getDay(prices[0].time);
            let hm = Time.getMonth(prices[0].time);
            let hv = [];
            for(let price of prices){
                if(Time.getDay(price.time) != hd){
                    hd = Time.getDay(price.time);
                    hv.push({price : max, time : Time.fullTime(price.time)});
                    max = price.high;
                }
                if(price.high > max){
                    max = price.high;
                }

            }
            return hv;
            break;
        case 'Close':
            let cv = [];
            let cm = Time.getMonth(prices[0].time);
            for(let i =0 ; i < prices.length-1;i++){
                if(Time.getDay(prices[i].time) !== Time.getDay(prices[i+1].time)){
                
                    cv.push({price : prices[i].close , time : Time.fullTime(prices[i].time)});
                }
            }
            return cv;
        case 'Volume':
            let vv = [];
            let volume = 0;
            let vm = Time.getMonth(prices[0].time);
            for(let J=0; J < prices.length-1; J++){
                    if(Time.getDay(prices[J].time) === Time.getDay(prices[J + 1].time)){
                        volume = prices[J].volume + prices[J+1].volume + volume;
                    }
                    else{
                       
                        vv.push(volume);
                        volume=0;
                    }
            }
            return vv;
            break;
        default:
            break;
    }
};

function calculateForMonth(prices, practicalPrice){
    switch (practicalPrice) {
        case 'Open':
            let month = Time.getMonth(prices[0].time);
            let values = [];
            for(let price of prices){
                if(Time.getMonth(price.time) != month){
                    month = Time.getMonth(price.time);
                    values.push({price : price.open, time: Time.fullTime(price.time)});
                }
            }
            return values;
            break;
        case 'Low':
            let min = prices[0].low;
            let ld = Time.getDay(prices[0].time);
            let lm = Time.getMonth(prices[0].time);
            let lv = [];
            for(let price of prices){
                if(Time.getMonth(price.time) != lm){
                    lm = Time.getMonth(price.time);
                    lv.push({price : min, time : Time.fullTime(price.time)});
                    min = price.low;
                }
                if(price.low < min){
                    min = price.low;
                }

            }
            return lv;
            break;
        case 'High':
            let max = prices[0].high;
            let hd = Time.getDay(prices[0].time);
            let hm = Time.getMonth(prices[0].time);
            let hv = [];
            for(let price of prices){
                if(Time.getMonth(price.time) != hm){
                    hm = Time.getMonth(price.time);
                    hv.push({price : max, time : Time.fullTime(price.time)});
                    max = price.high;
                }
                if(price.high > max){
                    max = price.high;
                }

            }
            return hv;
            break;
        case 'Close':
            let cv = [];
            let cm = Time.getMonth(prices[0].time);
            for(let i =0 ; i < prices.length-1;i++){
                if(Time.getMonth(prices[i].time) !== Time.getMonth(prices[i+1].time)){
                
                    cv.push({price : prices[i].close , time : Time.fullTime(prices[i].time)});
                }
            }
            return cv;
            break;    
        default:
            break;
        case 'Volume':
            let vv = [];
            let volume = 0;
            let vm = Time.getMonth(prices[0].time);
            for(let J=0; J < prices.length-1; J++){
                    if(Time.getMonth(prices[J].time) === Time.getMonth(prices[J + 1].time)){
                        volume = prices[J].volume + prices[J+1].volume + volume;
                    }
                    else{
                        vv.push(volume);
                        volume=0;
                    }
            }
            return vv;
            break;
    }
}

function calculateForWeek(prices, practicalPrice){
        switch (practicalPrice) {
            case 'Open':
                let day = Time.getDay(prices[0].time);
                let month = Time.getMonth(prices[0].time);
                let values = [];
                for(let price of prices){
                    if(Math.abs(Time.getDay(price.time) - day) >= 6){
                        day = Time.getDay(price.time);
                        values.push({price : price.open, time: Time.fullTime(price.time)});
                    }
            }
            return values;
                break;
            case 'Low':
                let min = prices[0].low;
                let ld = Time.getDay(prices[0].time);
                let lm = Time.getMonth(prices[0].time);
                let lv = [];
                for(let price of prices){
                    if(Math.abs(Time.getDay(price.time) - ld) >= 6){
                        ld = Time.getDay(price.time);
                        lv.push({price : min, time : Time.fullTime(price.time)});
                        min = price.low;
                    }
                    if(price.low < min){
                        min = price.low;
                    }

                }
                return lv;
                break;
            case 'High':
                let max = prices[0].high;
                let hd = Time.getDay(prices[0].time);
                let hm = Time.getMonth(prices[0].time);
                let hv = [];
                for(let price of prices){
                    if(Math.abs(Time.getDay(price.time) - hd) >= 6){
                        hd = Time.getDay(price.time);
                        hv.push({price : max, time : Time.fullTime(price.time)});
                        max = price.high;
                    }
                    if(price.high > max){
                        max = price.high;
                    }
    
                }
                return hv;
                break;
            case 'Close':
                case 'Close':
                let cv = [];
                let cd = Time.getDay(prices[0].time);
                let cm = Time.getMonth(prices[0].time);
                for(let price of prices){
                    if(Math.abs(Time.getDay(price.time) - cd) >= 6){
                        cd = Time.getDay(price.time);
                        cv.push({price : price.close , time : Time.fullTime(price.time)});
                    }
                }
                return cv;
                break;
            case 'Volume':
                let vv = [];
                let volume = 0;
                let vm = Time.getMonth(prices[0].time);
                for(let J=0; J < prices.length-1; J++){
                    if(Math.abs(Time.getDay(prices[J].time) -Time.getDay(prices[J + 1].time)) <6){
                        volume = prices[J].volume + prices[J+1].volume + volume;
                    }
                    else{
                        vv.push(volume);
                        volume=0;
                    }
                }
                return vv;
                break;
            default:
                break;
        }
}

function calculateForCurrent(prices, practicalPrice){
    switch (practicalPrice) {
        case 'Open':
            let day = Time.getDay(prices[0].time);
            let month = Time.getMonth(prices[0].time);
            let values = [];
            let times =0;
            for(let price of prices){
                if(values.length > 13){
                    break;
                }
                if(Time.getDay(price.time) != day){
                    day = Time.getDay(price.time);
                    values.push({price : price.open, time: Time.fullTime(price.time)});
                }
            }
            return values;
            break;
        case 'Low':
            let min = prices[0].low;
            let ld = Time.getDay(prices[0].time);
            let lm = Time.getMonth(prices[0].time);
            let lv = [];
            for(let price of prices){
                if(lv.length > 13){
                    break;
                }
                if(Time.getDay(price.time) != ld){
                    ld = Time.getDay(price.time);
                    lv.push({price : min, time : Time.fullTime(price.time)});
                    min = price.low;
                }
                if(price.low < min){
                    min = price.low;
                }

            }
            return lv;
            break;
        case 'High':
            let max = prices[0].high;
            let hd = Time.getDay(prices[0].time);
            let hm = Time.getMonth(prices[0].time);
            let hv = [];
            for(let price of prices){
                if(hv.length > 13){
                    break;
                }
                if(Time.getDay(price.time) != hd){
                    hd = Time.getDay(price.time);
                    hv.push({price : max, time : Time.fullTime(price.time)});
                    max = price.high;
                }
                if(price.high > max){
                    max = price.high;
                }

            }
            return hv;
            break;
        case 'Close':
            let cv = [];
            let cm = Time.getMonth(prices[0].time);
            let cd = Time.getDay(prices[0].time);
            for(let i =0 ; i < prices.length-1;i++){
                if(cv.length > 13){
                    break;
                }
                if(Time.getDay(prices[i].time) !== Time.getDay(prices[i+1].time)){
                
                    cv.push({price : prices[i].close , time : Time.fullTime(prices[i].time)});
                }
            }
            return cv;
        case 'Volume':
            let vv = [];
            let volume = 0;
            let vd = Time.getDay(prices[0].time);
            let vm = Time.getMonth(prices[0].time);
            for(let J=0; J < prices.length-1; J++){
                if(vv.length > 13){
                    break;
                }
                    if(Time.getDay(prices[J].time) === Time.getDay(prices[J + 1].time)){
                        volume = prices[J].volume + prices[J+1].volume + volume;
                    }
                    else{
                       
                        vv.push(volume);
                        volume=0;
                    }
            }
            return vv;
            break;
        default:
            break;
    }
}
module.exports = strategyRouter;