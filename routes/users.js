var express = require('express');
var userRouter = express.Router();
var bodyParser = require('body-parser');
var authentication = require('../authentication');
var passport = require('passport');
var cors = require('../routes/cors');
var User = require('../models/users');

/* GET users listing. */

function checkAdmin(id ){
      User.findById({_id : id}).then((user)=>{
        console.log('admin',user.admin);
        return true;  
      },(err) => next(err))
      .catch((err) => next(err));
};

userRouter.use(bodyParser.json());
userRouter.options('*' , cors.corsWithOptions,authentication.verifyUser, authentication.verifyAdmin , (req, res) => { res.statusCode = 200;})
userRouter.get('/', cors.corsWithOptions, (req, res,next) => {
  User.find({}).then((users) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(users);
  });
})
userRouter.route('/detail/:userId')
.get(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin, (req, res, next) => {
   User.findById(req.params.userId).then((user) => {
     res.statusCode = 200;
     res.setHeader('Content-Type', 'applicataion/json');
     res.json(user);
   }, (err) => next(err)).catch((err) => next(err))
})
userRouter.post('/signup' , cors.corsWithOptions, (req, res, next) => {
  User.findOne({email : req.body.email} , (err, resp) => {
    if(err){
      connsole.log('first err', err);
      return next(err);
    }
    else if(resp){
      console.log('resp', resp);
      let err = new Error('This username exists in database');
      res.statusCode = 500;
      res.setHeader('Content-Type', 'application/json');
      res.json({success : false, status: 'Registration Unsuccessful!', error : err.message});
    }
    else {
      User.register(new User({email : req.body.email}), req.body.password, (err , user) => {
        if(err){
            console.log('second err', err);
            res.statusCode = 500;
            res.setHeader('Content-Type', 'application/json');
            res.json({err : err});
        }
        else{
            console.log('success');
            passport.authenticate('local')(req, res, () =>{
              res.statusCode = 200;
              res.setHeader('Content-Type' , 'application/json');
              res.json({success: true , status : 'Registration Successful'});
          });
        }
    });
    }
  });
});

userRouter.post('/login' , cors.corsWithOptions , (req, res, next) =>{
  console.log('enter login');
  passport.authenticate('local' , (err, user, info) => {
    console.log('enter local');
    if(err){
      console.log('err from login',err);
      return next(err);
    }
    else if(!user){
      console.log('enterr login unsuccess');
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      res.json({success : false, status : 'Login Unsuccessful!', err : info})
    }
    else{
      req.logIn(user , (err) => {
        if(err){
          return next(err);
        }
        User.findById({_id : req.user._id}).then((user)=>{
          let token = authentication.getToken({_id : req.user._id});
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({success : true,  token : token, admin : user.admin});
        },(err) => next(err))
        .catch((err) => next(err));

      });
    }  
    
  })(req,res,next);
});

userRouter.route('/profile')
.options(cors.corsWithOptions, (req, res) => {
  console.log('im enter to options');
  res.statusCode =200;
})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
  User.findById({_id : req.user._id}).then((user) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(user);
  }, (err) => next(err))
  .catch((err) => next(err));
})
.put(cors.corsWithOptions , authentication.verifyUser, (req, res, next) => {
  let id ;
  if(req.body.id){
    console.log('id=',req.body.id);
    id = req.body.id
  }
  else{
    id =  req.user._id;
  }
  User.findByIdAndUpdate({_id :id},{$set : req.body},{new : true})
  .then((user) => {
    res.statusCode =200;
    res.setHeader('Content-Type', 'application/json');
    res.json(user);
  }, (err) => next(err))
  .catch((err) => next(err));
})

userRouter.route('/resetpassword')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.post(cors.corsWithOptions, authentication.verifyUser, (req, res, next) =>{
  User.findById({_id : req.user._id}).then((user) => {
    console.log(req.body.password);
    console.log(user);
    user.setPassword(req.body.password,(err , user) =>{
      if(err){
        console.log(err);
        return next(err);
      }
      if(user){
        console.log('user',user);
        user.save().then((user) => {
          res.statusCode =200;
          res.setHeader('Content-Type' , 'application/json');
          res.json(user);
        }).catch((err) => next(err))
      }
    });
  }, (err) => next(err))
  .catch((err) => next(err));
});
userRouter.route('/changeProfileImage')
.options(cors.corsWithOptions, (req, res) => { res.statusCode = 200;})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) =>{
    User.findByIdAndUpdate({_id : req.user._id},{image : req.body.image},{new:true})
    .then((user) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(user);
    },(err) => next(err))
    .catch((err) => next(err))
});
userRouter.route('/getProfileImage')
.options(cors.corsWithOptions, (req, res) => { res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    User.findById({_id: req.user._id}).then((user) =>{
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({image : user.image});
    }, (err) => next(err))
    .catch((err) => next(err))
});
userRouter.route('/wallet')
.options(cors.corsWithOptions, (req, res) => { res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    User.findById(req.user._id).then((user) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({wallet : user.wallet});
    }, (err) => next(err))
    .catch((err) => next(err));

})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
  User.findById(req.user._id).then((user) => {
    user.wallet = req.body.wallet;
    user.save().then((user) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(user);
    }, (err) => next(err)).catch(err => next(err))
  },(err) => next(err)).catch((err) => next(err))
})

userRouter.route('/digitalWallet/:currencyName')
.options(cors.corsWithOptions, (req, res) => { res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
 
  User.findById(req.user._id).then((user) => {
     let digitalCurrency = user.digitalWallet.find(digitalCurrency => (digitalCurrency.currencyName === req.params.currencyName))
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(digitalCurrency);
  }, (err) => next(err))
  .catch((err) => next(err));

})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
  User.findById(req.user._id).then((user) => {
    let digitalCurrency =user.digitalWallet.find(digitalCurrency => (digitalCurrency.currencyName === req.params.currencyName))
    if(digitalCurrency){
      digitalCurrency.value = req.body.value;
    }
    else{user.digitalWallet.push(req.body);}

    user.save().then((user) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(user);
    },(err) => next(err)).catch((err) => next(err))
  }, (err) => next(err)).catch((err) => next(err))
})

module.exports = userRouter;
