var express = require('express');
var bodyParser = require('body-parser');
var authenticate = require('../authentication');
var multer = require('multer');
var cors = require('./cors');

var fs = require('fs');

const uploadRouter = express.Router();

const newsStorage = multer.diskStorage({
    destination : (req, file, cb) => {
        cb(null, 'public/images/news');
    },
    filename : (req, file, cb) => {
        cb(null, file.originalname)
    }
});
const profileStorage = multer.diskStorage({
    destination : (req, file, cb) => {
        cb(null, 'public/images/profile');
    },
    filename : (req, file, cb) => {
        cb(null, file.originalname)
    }
});
const authStorage = multer.diskStorage({
    destination : (req, file, cb) => {
        if (!fs.existsSync('public/images/auth/'+req.user.id)){
            fs.mkdirSync('public/images/auth/'+req.user.id);
        }
        cb(null, 'public/images/auth/'+req.user.id);
    },
    filename : (req, file, cb) => {
        cb(null, file.originalname)
    }
});

const imageFilter = (req, file, cb) => {
    if(!file.originalname.match("/\.png|gif|jpg|jpeg|jfif/")){
        console.log('notmatch');
        return cb(new Error('You can upload only image file'));
    }
    else{
        console.log('match');
        return cb(null, true);
    }
}
const newsUpload = multer({storage : newsStorage, fileFilter : imageFilter});
const profileUpload = multer({storage : profileStorage, fileFilter : imageFilter})
const authUpload = multer({storage : authStorage, fileFilter : imageFilter});

uploadRouter.use(bodyParser.json());

uploadRouter.route('/profileimage')
.options(cors.corsWithOptions, (req, res) => {res.statusCode=200;console.log('true');})
.post(cors.corsWithOptions, authenticate.verifyUser, profileUpload.single('profileImage'),(req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(req.file);
});
uploadRouter.route('/newsimage')
.options(cors.corsWithOptions, (req, res) => {res.statusCode=200})
.post(cors.corsWithOptions, authenticate.verifyUser, newsUpload.single('newsImage'),(req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(req.files[0]);
});

uploadRouter.route('/authentication')
.options(cors.corsWithOptions, (req, res) => { res.statusCode = 200;
    fs.writeFile('/public/images/auth'+ req.user._id);
})
.post(cors.corsWithOptions, authenticate.verifyUser,authUpload.single('authImage') ,(req, res,next) => {
    console.log('uploadauthImage');
    res.statusCode = 200;   
    res.setHeader('Content-Type', 'application/json');
    res.json(req.file);
})
module.exports = uploadRouter; 