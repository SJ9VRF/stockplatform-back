var express = require('express');
var bodyParser = require('body-parser');
var authentication = require('../authentication');
var cors = require('../routes/cors');
var Currency = require('../models/digitalCurrency');
var Shoppers = require('../models/shoppers');
var Sellers = require('../models/sellers');

var currencyRouter = express.Router();

currencyRouter.use(bodyParser.json());


currencyRouter.route('/setData')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.post(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin , (req, res, next) => {
    Currency.insertMany(req.body,(err , Currency) => {
        if(err){
            return next(err);
        }
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(Currency);
    });
})
currencyRouter.route('/updateData/:currencyId/prices')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.put(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin, (req, res, next) => {
    Currency.findById({_id : req.params.currencyId}).then((currency) => {
        console.log('here');
        for(let price of req.body.prices){
            currency.prices.push(price);
        }
        console.log('currency.prices',currency.prices);
        currency.save().then((currency) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(currency);
        }, (err) => next(err)).catch(err => next(err))
    }, (err) => next(err))
    .catch((err) => next(err))
})
currencyRouter.route('/allTime/:name')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Currency.find({name : req.params.name}).then((currency) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(currency);
    })
})
/*currencyRouter.route('/now/:currencyName')
.post(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    let date = new Date();
    console.log('date' , date);
    Currency.find({name : req.params.currencyName}).then((currency) => {
        currency.prices.find((price) => price.timestamps === date)
        res.statusCode = 200;
        res.setHeader('Content-Type','application/json');
        res.json(currency);
    })
})*/

currencyRouter.route('/shoppersList/:currencyName')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser , (req, res, next) => {
    Shoppers.find({currencyName : req.params.currencyName}).then((shoppers) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json ');
        res.json(shoppers);
    }, (err) => next(err))
    .catch(err => next(err));
})
.post(cors.corsWithOptions, authentication.verifyUser , (req, res, next) => {
    Shoppers.create(req.body).then((shopper) =>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(shopper);
    }, (err) => next(err))
    .catch((err) => next(err))
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin , (req, res, next) => {
    Shoppers.remove({currencyName : req.params.currencyName}).then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    },(err) => next(err))
    .catch((err) => next(err));    
})
currencyRouter.route('/shoppersList/:currencyName/orders/:orderId')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Shoppers.findOne({currencyName : req.params.currencyName}).then((shoppers) => {
        console.log('shoppers', shoppers);
        console.log('params id', req.params.orderId);
       let order =shoppers.orders.find((ord) => ord._id == req.params.orderId) 
       if(req.body.value){
           order.value = req.body.value;
       }
       if(req.body.unitPrice){
           order.unitPrice = req.body.unitPrice;
       }
       shoppers.save().then((shoppers) => {
           res.statusCode =200;
           res.setHeader('Content-Type','application/json');
           res.json(shoppers);
       }, (err) => next(err)).catch(err => next(err))
    }, (err) => next(err)).catch((err) => next(err))
})
currencyRouter.route('/shoppersList/:currencyName/orders')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.post(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Shoppers.findOne({currencyName : req.params.currencyName}).then((shoppers) => {
        if(shoppers){
            console.log('sellers', shoppers);
            console.log('seller orders', shoppers.orders);
            req.body.shopper = req.user._id;
            shoppers.orders.push(req.body);
            shoppers.save().then((shoppers) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(shoppers);
            }, (err) => next(err)).catch((err) => next(err))
        }
        else{
            Shoppers.create({currencyName : req.params.currencyName}).then((shoppers) => {
                req.body.shopper = req.user._id;
                shoppers.orders = req.body;
                shoppers.save().then((shoppers) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(shoppers);    
                },(err) => next(err)).catch((err) => next(err))
            },(err) => next(err)).catch((err) => next(err))
        }
    }, (err) => next(err)).catch((err) => next(err))
})
/********************************************** */
currencyRouter.route('/sellersList/:currencyName')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.get(cors.corsWithOptions, authentication.verifyUser , (req, res, next) => {
    Sellers.find({currencyName : req.params.currencyName}).then((sellers) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json ');
        res.json(sellers);
    }, (err) => next(err))
    .catch(err => next(err));
})
.post(cors.corsWithOptions, authentication.verifyUser , (req, res, next) => {
    Sellers.create(req.body).then((seller) =>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(seller);
    }, (err) => next(err))
    .catch((err) => next(err))
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin , (req, res, next) => {
    Sellers.remove({currencyName : req.params.currencyName}).then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    },(err) => next(err))
    .catch((err) => next(err));    
})
currencyRouter.route('/sellersList/:currencyName/orders/:orderId')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Sellers.findOne({currencyName : req.params.currencyName}).then((sellers) => {
        console.log('sellers', sellers);
        console.log('params id', req.params.orderId);
       let order =sellers.orders.find((ord) => ord._id == req.params.orderId) 
       if(req.body.value){
           order.value = req.body.value;
       }
       if(req.body.unitPrice){
           order.unitPrice = req.body.unitPrice;
       }
       sellers.save().then((sellers) => {
           res.statusCode =200;
           res.setHeader('Content-Type','application/json');
           res.json(sellers);
       }, (err) => next(err)).catch(err => next(err))
    }, (err) => next(err)).catch((err) => next(err))
})
currencyRouter.route('/sellersList/:currencyName/orders')
.options(cors.corsWithOptions, (req, res) => {res.statusCode = 200;})
.post(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    Sellers.findOne({currencyName : req.params.currencyName}).then((sellers) => {
        if(sellers){
            console.log('sellers', sellers);
            console.log('seller orders', sellers.orders);
            req.body.seller = req.user._id;
            sellers.orders.push(req.body);
            sellers.save().then((sellers) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(sellers);
            }, (err) => next(err)).catch((err) => next(err))
        }
        else{
            Sellers.create({currencyName : req.params.currencyName}).then((sellers) => {
                req.body.seller = req.user._id;
                sellers.orders = req.body;
                sellers.save().then((sellers) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(sellers);    
                },(err) => next(err)).catch((err) => next(err))
            },(err) => next(err)).catch((err) => next(err))
        }
     
    }, (err) => next(err)).catch((err) => next(err))
})
module.exports = currencyRouter;