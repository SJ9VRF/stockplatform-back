const express = require('express');
const bodyParser = require('body-parser');
const cors = require('./cors');
const passport = require('passport');
const authentication = require('../authentication');
const News = require('../models/news');

const newsRouter = express.Router();

newsRouter.use(bodyParser.json());

newsRouter.options('*' , cors.corsWithOptions , (req, res) => {
    res.statusCode = 200;
});
newsRouter.route('/lang/:langName')
.options(cors.corsWithOptions , (req,res) => { res.statusCode = 200})
.get(cors.cors , (req, res, next) => {
    News.find({}).populate('comments.author').then((news) => {
        if(req.params.langName === 'en'){
            news = news.map((news) => {return {title : news.titleEn, content : news.contentEn, image : news.image, likes : news.likes,
            hates : news.hates, comments : news.comments, _id : news._id}})
        }
        else{
            news = news.map((news) => {return {title : news.title, content : news.content, image : news.image, likes : news.likes,
                hates : news.hates, comments : news.comments, _id : news._id}})
        }
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions,authentication.verifyUser, authentication.verifyAdmin,
(req, res, next) => {
    News.create(req.body).then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin,
(req, res, next) => {
    News.remove({}).then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

/*newsRouter.route('/:newsId')
.get(cors.cors , (req, res, next) => {
    News.findById({_id : req.params.newsId})
    .then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type' , 'applocation/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions,authentication.verifyUser, authentication.verifyAdmin ,(req, res, next) => {
    News.findByIdAndUpdate({_id : req.params.newsId},{$set : req.body},{new : true})
    .then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type' , 'application/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin ,
(req, res, next) => {
    News.findByIdAndRemove({_id : req.params.newsId})
    .then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type' , 'application/josn');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
});*/
newsRouter.route('/:newsId/lang/:langName')
.options(cors.corsWithOptions , (req,res) => { res.statusCode = 200})
.get(cors.corsWithOptions , (req, res, next) => {
    News.findById(req.params.newsId).populate('comments.author').then((news) => {
        if(req.params.langName === 'en'){
            news = {title : news.titleEn, content : news.contentEn, image : news.image, likes : news.likes,
            hates : news.hates, comments : news.comments, _id : news._id};
        }
        else{
            news = {title : news.title, content : news.content, image : news.image, likes : news.likes,
                hates : news.hates, comments : news.comments, _id : news._id};
        }
        console.log('news populate ger', news);
        console.log('yoojo');
        res.statusCode=200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin, (req, res, next) =>{
    News.findByIdAndUpdate({_id : req.params.newsId},{$set : req.body},{new: true})
    .then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news);
    }, (err) => next(err))
    .catch((err) => next(err))
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin, (req, res, next) => {
    News.findByIdAndRemove({_id : req.params.newsId}, (err, resp) =>{
        if(err){
            return next(err);
        }
        res.statusCode=200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    });
})
newsRouter.route('/:newsId/comments')
.options(cors.corsWithOptions , (req,res) => { res.statusCode = 200})
.get(cors.corsWithOptions, (req, res, next) => {
    News.findById({_id : req.params.newsId}).populate('comments.author')
    .then((news) => {
        if(news != null){
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(news);
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    News.findById({_id : req.params.newsId}).
    then((news) => {
        req.body.author = req.user._id;
        news.comments.push(req.body);
        news.save().then((news) => {
            console.log('new news', news);
            News.findById({_id : news._id}).populate('comments.author')
            .then((news) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(news);
            }).catch((err) => next(err));
        }).catch((err) => next(err))
    }).catch((err) => next(err))
})
.delete(cors.corsWithOptions, authentication.verifyUser, authentication.verifyAdmin, (req, res, next) => {
    News.findById(req.params.newsId).then((news) => {
        for( var i = (news.comments.length) - 1 ; i >= 0 ; i--){
            news.comments.id(news.comments[i]._id).remove();
        }
        news.save().then((news) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(news);
        }, (err) => next(err)).catch((err) => next(err))
    }, (err) => next(err)).catch((err) => next(err));
})
newsRouter.route('/:newsId/comments/:commentId')
.options(cors.corsWithOptions , (req,res) => { res.statusCode = 200})
.get(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    News.findById(req.params.newsId).populate('comments.author').then((news) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(news.comments.id(req.params.commentId));
        
    }, (err) => next(err)).catch((err) => next(err));
})
.put(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    News.findById(req.params.newsId).then((news) =>{
        let comment= news.comments.id(req.params.commentId)
        console.log('comment', comment);
        if((req.user._id).equals(comment.author)){
            console.log('yse', req.body);
            if(req.body.content){ comment.content = req.body.content; console.log('new comment', comment);}
            news.save().then((news) => {
                News.findById(news._id).populate('comments.author').then((news) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(news);
                }).catch((err) => next(err))
               
            }, (err) => next(err))
            .catch((err) => next(err))
        }
        else{
            res.sendStatus = 404;
            res.setHeader('Content-Type', 'application/json');
            res.send('this comment does not blong to you');
        }
    }, (err) => next(err))
    .catch((err) => next(err))
})
.delete(cors.corsWithOptions, authentication.verifyUser, (req, res, next) => {
    News.findById(req.params.newsId).then((news) =>{
        let comment = news.comments.id(req.params.commentId);
        if((req.user._id).equals(comment.author)){
            comment.remove();
            news.save().then((news) => {
                News.findById(news._id).populate('comments.author').then((news) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(news);
                },(err) => next(err)).catch((err) => next(err))
            }, (err) => next(err)).catch((err) => next(err));
        }
        else{
            console.log(req.user._id, '   ' , comment.author);
            var err = new Error('You are not authorized to delete this comment!');
            err.status = 403;
            return next(err);
        }
    }, (err) => next(err)).catch((err) => next(err))
})

module.exports = newsRouter;